package io.fberbert.githubbrowser.data

import io.fberbert.githubbrowser.domain.model.Repository
import io.reactivex.Observable

interface GithubService {
  fun searchRepos(query: String): Observable<List<Repository>>
  fun getRepo(owner: String, repo: String): Observable<Repository>
}