package io.fberbert.githubbrowser.data.model

data class RepositoryEntity(val id: Long,
                            val name: String,
                            val owner: UserEntity,
                            val description: String,
                            val forks_count: Int,
                            val subscribers_count: Int,
                            val subscribers_url: String){

  override fun toString(): String {
    return "RepositoryEntity(id=$id, name='$name', owner=$owner, description='$description', forks_count=$forks_count, subscribers_count=$subscribers_count, subscribers_url='$subscribers_url')"
  }
}