package io.fberbert.githubbrowser.data.model

import io.fberbert.githubbrowser.data.model.RepositoryEntity

data class SearchRepoResponseEntity(val total_count: Int, val incomplete_results: Boolean, val items: List<RepositoryEntity>)