package io.fberbert.githubbrowser.data.model

data class UserEntity(val id: Long, val login: String, val avatar_url: String) {

  override fun toString(): String {
    return "UserEntity(id=$id, login='$login', avatar_url='$avatar_url')"
  }
}