package io.fberbert.githubbrowser.di.component

import io.fberbert.githubbrowser.App
import io.fberbert.githubbrowser.di.module.ApplicationModule
import io.fberbert.githubbrowser.di.module.BuildersModule
import io.fberbert.githubbrowser.di.module.NetModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetModule::class, BuildersModule::class))
interface ApplicationComponent {
  @Component.Builder
  interface Builder {
    @BindsInstance
    fun application(application: App): Builder

    fun build(): ApplicationComponent
  }
  fun inject(app: App)
}
