package io.fberbert.githubbrowser.di.module

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import io.fberbert.githubbrowser.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {

  @Provides
  @Singleton
  fun provideContext(app: App): Context = app.applicationContext

  @Provides
  @Singleton
  fun provideSharedPreferences(app: App): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)
}
