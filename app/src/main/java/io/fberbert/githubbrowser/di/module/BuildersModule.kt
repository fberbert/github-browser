package io.fberbert.githubbrowser.di.module

import io.fberbert.githubbrowser.ui.repodetails.RepoDetailActivity
import io.fberbert.githubbrowser.ui.repolist.RepoListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class BuildersModule {

  @ContributesAndroidInjector(modules = arrayOf(RepoListModule::class))
  internal abstract fun bindRepoListActivity(): RepoListActivity

@ContributesAndroidInjector(modules = arrayOf(RepoDetailModule::class))
  internal abstract fun bindRepoDetailActivity(): RepoDetailActivity

}