package io.fberbert.githubbrowser.di.module

import io.fberbert.githubbrowser.data.GithubService
import io.fberbert.githubbrowser.ui.repodetails.RepoDetailViewModelFactory
import dagger.Module
import dagger.Provides


@Module
class RepoDetailModule {

  @Provides
  fun provideRepoDetailViewModelFactory(githubService: GithubService): RepoDetailViewModelFactory {
    return RepoDetailViewModelFactory(githubService)
  }

}