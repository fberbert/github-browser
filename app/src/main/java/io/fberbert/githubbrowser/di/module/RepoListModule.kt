package io.fberbert.githubbrowser.di.module

import io.fberbert.githubbrowser.data.GithubService
import io.fberbert.githubbrowser.ui.repolist.RepoListViewModel
import dagger.Module
import dagger.Provides


@Module
class RepoListModule {

  @Provides
  fun provideRepoListViewModelFactory(githubService: GithubService): RepoListViewModel.RepoListViewModelFactory {
    return RepoListViewModel.RepoListViewModelFactory(githubService)
  }

}