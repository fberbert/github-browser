package io.fberbert.githubbrowser.di.module

import android.os.Environment
import io.fberbert.githubbrowser.domain.GithubApiService
import io.fberbert.githubbrowser.data.GithubService
import io.fberbert.githubbrowser.domain.GithubServiceImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton
import okhttp3.Cache
import java.util.concurrent.TimeUnit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import com.google.gson.Gson
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber


@Module
class NetModule {
  @Singleton
  @Provides
  fun provideLogger(): HttpLoggingInterceptor.Logger {
    return HttpLoggingInterceptor.Logger { message ->
      Timber.tag("OkHttp")
      Timber.d(message)
    }
  }

  @Singleton
  @Provides
  fun provideHttpLoggingInterceptor(logger: HttpLoggingInterceptor.Logger): HttpLoggingInterceptor {
    val interceptor = HttpLoggingInterceptor(logger)
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    return interceptor
  }

  @Singleton
  @Provides
  @Named("cached")
  fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
    val cache = Cache(Environment.getDownloadCacheDirectory(), 10 * 1024 * 1024)
    return OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .readTimeout(1, TimeUnit.MINUTES)
        .writeTimeout(1, TimeUnit.MINUTES)
        .cache(cache)
        .build()
  }

  @Provides
  @Singleton
  fun provideGson(): Gson {
    val gsonBuilder = GsonBuilder()
    gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
    return gsonBuilder.create()
  }

  @Singleton
  @Provides
  fun provideRetrofit(gson: Gson, @Named("cached") client: OkHttpClient): Retrofit.Builder {
    return Retrofit.Builder()
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
  }

  @Provides
  @Singleton
  fun provideGithubApiService(builder: Retrofit.Builder): GithubApiService {
    return builder.baseUrl("https://api.github.com")
        .build()
        .create(GithubApiService::class.java)
  }

  @Provides
  @Singleton
  fun provideGithubService(githubApiService: GithubApiService): GithubService {
    return GithubServiceImpl(githubApiService)
  }
}