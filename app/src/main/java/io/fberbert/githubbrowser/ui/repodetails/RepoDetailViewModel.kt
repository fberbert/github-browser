package io.fberbert.githubbrowser.ui.repodetails

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import io.fberbert.githubbrowser.core.UiResponse
import io.fberbert.githubbrowser.data.GithubService
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class RepoDetailViewModel(val githubService: GithubService,
                          ioThread: Scheduler = Schedulers.io(),
                          mainThread: Scheduler = AndroidSchedulers.mainThread()) : ViewModel() {

  private val disposable = CompositeDisposable()
  private val response = MutableLiveData<UiResponse>()
  private val queryRelay = PublishRelay.create<Pair<String, String>>()

  init {
    disposable.add(
        queryRelay
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .doOnNext { response.value = UiResponse.Loading }
            .switchMap {
              githubService
                  .getRepo(it.first, it.second)
                  .subscribeOn(ioThread)
                  .observeOn(mainThread)
                  .map { repository ->
                    if (repository != null) {
                      UiResponse.Success(repository)
                    } else {
                      UiResponse.NoContent
                    }
                  }
            }.onErrorReturn { t: Throwable -> UiResponse.Error(t) }
            .subscribe { uiResponse ->
              response.value = uiResponse
            })
  }

  override fun onCleared() {
    Timber.d("RepoDetailViewModel: onCleared()")
    disposable.clear()
  }

  fun response(): MutableLiveData<UiResponse> = response

  fun getRepo(owner: String, repo: String) {
    Timber.d("RepoListViewModel: getRepo($owner, $repo)")
    queryRelay.accept(Pair(owner, repo))
  }

}

class RepoDetailViewModelFactory(val githubService: GithubService) : ViewModelProvider.Factory {
  override fun <T : ViewModel?> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(RepoDetailViewModel::class.java)) {
      return RepoDetailViewModel(githubService) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}