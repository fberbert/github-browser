package io.fberbert.githubbrowser.ui.repodetails

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import io.fberbert.githubbrowser.core.UiResponse
import io.fberbert.githubbrowser.domain.model.Repository
import butterknife.BindView
import butterknife.ButterKnife
import dagger.android.AndroidInjection
import io.fberbert.githubbrowser.R
import javax.inject.Inject

class RepoDetailActivity: AppCompatActivity() {

  companion object {
    private val KEY_OWNER = "KEY_OWNER"
    private val KEY_REPO = "KEY_REPO"
    public val RESULT_NO_CONTENT = 111
    public val RESULT_ERROR = 222

    fun newIntent(context: Context, owner: String, repo: String): Intent {
      val intent = Intent(context, RepoDetailActivity::class.java)
      intent.putExtra(KEY_OWNER, owner)
      intent.putExtra(KEY_REPO, repo)
      return intent
    }
  }

  @Inject
  lateinit var repoDetailViewModelFactory: RepoDetailViewModelFactory

  @BindView(R.id.repo_name)
  lateinit var repositoryName: TextView
  @BindView(R.id.subscriber_count)
  lateinit var subscriberCount: TextView
  @BindView(R.id.progress_bar)
  lateinit var progressBar: ProgressBar
  @BindView(R.id.subscriber_list)
  lateinit var subscriberRecyclerView: RecyclerView
  @BindView(R.id.toolbar)
  lateinit var toolbar: Toolbar

  private lateinit var viewModel: RepoDetailViewModel

  private val adapter = SubscriberAdapter()


  override fun onCreate(savedInstanceState: Bundle?) {
    AndroidInjection.inject(this)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_repo_detail)
    ButterKnife.bind(this)

    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)

    setupSubscriberRecyclerView()
    viewModel = ViewModelProviders.of(this, repoDetailViewModelFactory).get(RepoDetailViewModel::class.java)
    viewModel.response().observe(this, Observer { processResponse(it) })

    if (intent.extras.getString(KEY_OWNER) != null && intent.extras.getString(KEY_REPO) != null) {
      viewModel.getRepo(intent.extras.getString(KEY_OWNER), intent.extras.getString(KEY_REPO))
    }
  }

  private fun setupSubscriberRecyclerView() {
    val dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
    subscriberRecyclerView.addItemDecoration(dividerItemDecoration)
    subscriberRecyclerView.setHasFixedSize(true)
    subscriberRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    subscriberRecyclerView.adapter = adapter
  }

  private fun processResponse(response: UiResponse?) {
    progressBar.visibility = View.GONE
    when (response) {
      is UiResponse.Loading -> progressBar.visibility = View.VISIBLE
      is UiResponse.Success<*> -> {
        val repository = response.data as Repository?
        repositoryName.text = repository?.name
        subscriberCount.text = getString(R.string.subscriber_count_label, repository?.subscribersCount)
        adapter.items = repository?.subscriberList
      }
      is UiResponse.Error -> {
        setResult(RESULT_ERROR)
        finish()
      }
      is UiResponse.NoContent -> {
        setResult(RESULT_NO_CONTENT)
        finish()
      }
    }
  }

  override fun onSupportNavigateUp(): Boolean {
    onBackPressed()
    return true
  }
}