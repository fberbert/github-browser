package io.fberbert.githubbrowser.ui.repolist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import io.fberbert.githubbrowser.core.UiResponse
import io.fberbert.githubbrowser.domain.model.Repository
import io.fberbert.githubbrowser.ui.repodetails.RepoDetailActivity
import io.fberbert.githubbrowser.utils.extensions.toast
import butterknife.BindView
import butterknife.ButterKnife
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.jakewharton.rxrelay2.PublishRelay
import dagger.android.AndroidInjection
import io.fberbert.githubbrowser.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class RepoListActivity : AppCompatActivity() {

  companion object {
    private val REQUEST_CODE_DETAIL = 1337
  }

  @Inject
  lateinit var repoListViewModelFactory: RepoListViewModel.RepoListViewModelFactory

  private lateinit var viewModel: RepoListViewModel

  val repoClickRelay = PublishRelay.create<Pair<String?, String?>>()
  private val adapter = RepositoryAdapter(repoClickRelay)

  @BindView(R.id.repo_list)
  lateinit var repoRecyclerView: RecyclerView
  @BindView(R.id.repos_search)
  lateinit var searchRepos: SearchView
  @BindView(R.id.progress_bar)
  lateinit var progressBar: ProgressBar
  @BindView(R.id.error_message)
  lateinit var errorMessage: TextView
  @BindView(R.id.empty_message)
  lateinit var emptyMessage: TextView
  @BindView(R.id.toolbar)
  lateinit var toolbar: Toolbar

  override fun onCreate(savedInstanceState: Bundle?) {
    AndroidInjection.inject(this)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_repo_list)
    ButterKnife.bind(this)

    setSupportActionBar(toolbar)

    setupRepoRecyclerView()

    viewModel = ViewModelProviders.of(this, repoListViewModelFactory).get(RepoListViewModel::class.java)
    viewModel.response().observe(this, Observer { processResponse(it) })

    RxSearchView.queryTextChanges(searchRepos)
        .filter { it.isNotEmpty() }
        .debounce(400, TimeUnit.MILLISECONDS, Schedulers.computation())
        .subscribe { viewModel.searchRepo(it.toString()) }
  }

  private fun setupRepoRecyclerView() {
    val dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager.VERTICAL)
    repoRecyclerView.addItemDecoration(dividerItemDecoration)
    repoRecyclerView.setHasFixedSize(true)
    repoRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    repoRecyclerView.adapter = adapter
    repoClickRelay
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
          if (it.first != null && it.second != null) {
            startActivityForResult(RepoDetailActivity.newIntent(this, it.first!!, it.second!!), REQUEST_CODE_DETAIL)
          } else {
            toast("Arguments missing")
          }
        }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == REQUEST_CODE_DETAIL) {
      if (resultCode == RepoDetailActivity.RESULT_NO_CONTENT || resultCode == RepoDetailActivity.RESULT_ERROR) {
          toast(getString(R.string.repo_list_error))
      }
    }
  }

  private fun processResponse(response: UiResponse?) {
    progressBar.visibility = View.GONE
    errorMessage.visibility = View.GONE
    emptyMessage.visibility = View.GONE
    when (response) {
      is UiResponse.Loading -> progressBar.visibility = View.VISIBLE
      is UiResponse.Success<*> -> {
        adapter.items = response.data as List<Repository>?
      }
      is UiResponse.Error -> {
        adapter.items = listOf()
        errorMessage.visibility = View.VISIBLE
      }
      is UiResponse.NoContent -> {
        adapter.items = listOf()
        emptyMessage.visibility = View.VISIBLE
      }
    }
  }
}