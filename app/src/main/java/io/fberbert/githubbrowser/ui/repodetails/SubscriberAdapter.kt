package io.fberbert.githubbrowser.ui.repodetails

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.fberbert.githubbrowser.R
import io.fberbert.githubbrowser.domain.model.User
import io.fberbert.githubbrowser.utils.extensions.load


class SubscriberAdapter : RecyclerView.Adapter<SubscriberAdapter.ViewHolder>() {
  var items: List<User>? = null
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_subscriber, parent, false))
  }

  override fun getItemCount() = items?.size ?: 0

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    items?.let {
      holder.bind(it[position])
    }
  }

  class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val avatar: ImageView = view.findViewById(R.id.avatar)
    private val login: TextView = view.findViewById(R.id.login)

    fun bind(subscriber: User) {
      subscriber.avatarUrl?.let { avatar.load(it) }
      avatar.contentDescription = avatar.context.getString(R.string.content_description_avatar, subscriber.login)
      login.text = subscriber.login
      }
  }
}
