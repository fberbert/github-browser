package io.fberbert.githubbrowser.ui.repolist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.fberbert.githubbrowser.domain.model.Repository
import io.fberbert.githubbrowser.utils.extensions.load
import com.jakewharton.rxrelay2.Relay
import io.fberbert.githubbrowser.R
import timber.log.Timber

class RepositoryAdapter(val clickRelay: Relay<Pair<String?, String?>>) : RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {
  var items: List<Repository>? = null
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false))
  }

  override fun getItemCount() = items?.size ?: 0

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    items?.let {
      holder.bind(it[position], clickRelay)
    }
  }

  class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    private val avatar: ImageView = view.findViewById(R.id.avatar)
    private val title: TextView = view.findViewById(R.id.title)
    private val description: TextView = view.findViewById(R.id.description)
    private val forksNumber: TextView = view.findViewById(R.id.forks_number)

    fun bind(repository: Repository, clickRelay: Relay<Pair<String?, String?>>) {
      repository.owner?.avatarUrl?.let { avatar.load(it) }
      avatar.contentDescription = avatar.context.getString(R.string.content_description_avatar, repository.owner?.login)
      title.text = repository.name
      description.text = repository.description
      forksNumber.text = repository.numberOfForks.toString()
      view.setOnClickListener {
        Timber.d("Repo %s clicked", repository.name)
        clickRelay.accept(Pair(repository.owner?.login, repository.name)) }
      avatar.setOnClickListener { clickRelay.accept(Pair(repository.owner?.login, repository.name)) }
    }
  }
}
