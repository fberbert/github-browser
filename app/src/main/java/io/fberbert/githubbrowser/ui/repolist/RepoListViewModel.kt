package io.fberbert.githubbrowser.ui.repolist

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import io.fberbert.githubbrowser.core.UiResponse
import io.fberbert.githubbrowser.data.GithubService
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class RepoListViewModel(val githubService: GithubService,
                        ioThread: Scheduler = Schedulers.io(),
                        mainThread: Scheduler = AndroidSchedulers.mainThread()) : ViewModel() {

  private val disposable = CompositeDisposable()
  private val response = MutableLiveData<UiResponse>()
  private val queryRelay = PublishRelay.create<String>()

  init {
    disposable.add(
        queryRelay
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .doOnNext { response.value = UiResponse.Loading }
            .switchMap {
              githubService.searchRepos(it)
                  .map {
                    repoList ->
                    if (repoList.isEmpty()) {
                      UiResponse.NoContent
                    } else {
                      UiResponse.Success(repoList)
                    }
                  }
                  .onErrorResumeNext { t: Throwable -> Observable.just(UiResponse.Error(t)) }
            }
            .subscribe { uiResponse ->
              response.value = uiResponse
            })
  }

  override fun onCleared() {
    Timber.d("RepoListViewModel: onCleared()")
    disposable.clear()
  }

  fun response(): MutableLiveData<UiResponse> = response

  fun searchRepo(query: String) {
    Timber.d("RepoListViewModel: searchRepo($query)")
    queryRelay.accept(query)
  }

  class RepoListViewModelFactory(val githubService: GithubService) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
      if (modelClass.isAssignableFrom(RepoListViewModel::class.java)) {
        return RepoListViewModel(githubService) as T
      }
      throw IllegalArgumentException("Unknown ViewModel class")
    }
  }
}