package io.fberbert.githubbrowser.domain.model

data class Repository(val name: String?,
                      val description: String?,
                      val numberOfForks: Int?,
                      val owner: User?,
                      val subscribersCount: Int?,
                      val subscriberList: List<User>? = null) {

  override fun toString(): String {
    return "Repository(name=$name, description=$description, numberOfForks=$numberOfForks, owner=$owner)"
  }
}