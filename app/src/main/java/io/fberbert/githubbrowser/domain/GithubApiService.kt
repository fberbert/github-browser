package io.fberbert.githubbrowser.domain

import io.fberbert.githubbrowser.data.model.RepositoryEntity
import io.fberbert.githubbrowser.data.model.SearchRepoResponseEntity
import io.fberbert.githubbrowser.data.model.UserEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface GithubApiService {

  @GET("/search/repositories")
  fun searchRepos(@Query("q") query: String): Observable<SearchRepoResponseEntity>

  @GET("/repos/{owner}/{repo}")
  fun getRepo(@Path("owner") owner: String, @Path("repo") repo: String): Observable<RepositoryEntity>

  @GET
  fun getSubscriberList(@Url url: String): Observable<List<UserEntity>>

}