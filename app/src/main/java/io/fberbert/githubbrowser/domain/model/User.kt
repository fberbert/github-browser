package io.fberbert.githubbrowser.domain.model

data class User(val login: String?, val avatarUrl: String?)