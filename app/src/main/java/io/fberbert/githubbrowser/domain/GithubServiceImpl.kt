package io.fberbert.githubbrowser.domain

import io.fberbert.githubbrowser.data.GithubService
import io.fberbert.githubbrowser.domain.model.Repository
import io.fberbert.githubbrowser.domain.model.User
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GithubServiceImpl(val githubApiService: GithubApiService,
                        private val ioThread: Scheduler = Schedulers.io(),
                        private val mainThread: Scheduler = AndroidSchedulers.mainThread()) : GithubService {

  override fun searchRepos(query: String): Observable<List<Repository>> {
    return githubApiService.searchRepos(query)
        .subscribeOn(ioThread)
        .observeOn(mainThread)
        .map { searchRepoResponseEntity ->
          List(searchRepoResponseEntity.items.size)
          { index ->
            searchRepoResponseEntity.items[index].let {
              Repository(it.name, it.description, it.forks_count, User(it.owner.login, it.owner.avatar_url), it.subscribers_count)
            }
          }
        }
  }

  override fun getRepo(owner: String, repo: String): Observable<Repository> {
    return githubApiService.getRepo(owner, repo)
        .subscribeOn(ioThread)
        .observeOn(ioThread)
        .flatMap { repositoryEntity ->
          githubApiService
              .getSubscriberList(repositoryEntity.subscribers_url)
              .subscribeOn(ioThread)
              .observeOn(mainThread)
              .map { subscriberEntityList ->
                List(subscriberEntityList.size) { index ->
                  subscriberEntityList[index].let {
                    User(it.login, it.avatar_url)
                  }
                }
              }
              .map { subscriberList ->
                Repository(repositoryEntity.name,
                    repositoryEntity.description,
                    repositoryEntity.forks_count,
                    User(repositoryEntity.owner.login, repositoryEntity.owner.avatar_url),
                    repositoryEntity.subscribers_count,
                    subscriberList)
              }
        }
  }
}