package io.fberbert.githubbrowser

import android.app.Activity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.fberbert.githubbrowser.di.component.DaggerApplicationComponent
import timber.log.Timber
import javax.inject.Inject


class App : android.app.Application(), HasActivityInjector {

  @Inject
  lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

  override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

  override fun onCreate() {
    super.onCreate()
    Timber.plant(Timber.DebugTree())
    DaggerApplicationComponent
        .builder()
        .application(this)
        .build()
        .inject(this)
  }
}

