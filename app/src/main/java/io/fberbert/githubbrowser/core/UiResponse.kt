package io.fberbert.githubbrowser.core

sealed class UiResponse {
  object Loading : UiResponse()
  object NoContent : UiResponse()
  data class Success<T>(val data: T) : UiResponse()
  data class Error(val error: Throwable) : UiResponse()

}